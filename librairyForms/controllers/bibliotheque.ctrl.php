<?php
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/forms.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/pattern.php';
session_start();

$database = new database();
$connectDB = $database->connectDB();
$forms = new form($connectDB);
$forms->userId = $_SESSION['idUser'];

$pattern = new pattern($connectDB);
//récupére la liste des formes personnelles des utilisateurs
$formsUser = $forms->getFormForUser();
//récupére la liste des formes communes

//Supprime une forme
if(isset($_POST['idFormDelete'])){
  $forms->idForm = $_POST['idFormDelete'];
  $pattern->formId = $forms->idForm;
  $pattern->deletePattern();
  $forms->deleteForm();
  header('Location:bibliotheque.php');
}
?>
