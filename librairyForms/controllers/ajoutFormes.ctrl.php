<?php
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/forms.class.php';
session_start();

$database = new database();
$connectDB = $database->connectDB();
$form = new form($connectDB);
$form->userId = $_SESSION['idUser'];

$regexName = '/[A-Za-z0-9\s]/';
$regexLink = '/(http:\/\/|https:\/\/|www.)([a-zA-Z0-9-_?=+!"]+\.)+([a-z]+)/';

if(count($_POST) > 0){
  //Stock l'id de la forme depuis la bibliotheque
  if(isset($_POST['idFormEdit'])){
    $form->idForm = $_POST['idFormEdit'];
    //Remplit les attributs correspondant à l'id de la forme
    $hum = $form->loadFormById();
    var_dump($hum);
  }else if(isset($_POST['editForm']) && isset($_POST['idForm'])){
    $form->idForm = $_POST['idForm'];
  }

  //pour l'ajout et l'edition de formes
  if(isset($_POST['addForm']) || isset($_POST['editForm'])){

  //Verifie l'existence et stock les valeurs du formulaire d'ajout de formes
  if(isset($_POST['sizeHook'])){
    $sizeHook = htmlspecialchars($_POST['sizeHook']);
    $form->sizeHook = $sizeHook;
  }
  if(isset($_POST['sizeYarn'])){
    $sizeYarn = htmlspecialchars($_POST['sizeYarn']);
    $form->sizeYarn = $sizeYarn;
  }
  if(isset($_POST['numberColors'])){
    $numberColors = htmlspecialchars($_POST['numberColors']);
    $form->numberColors = $numberColors;
  }
  if(isset($_POST['publicForm'])){
    $publicForm = htmlspecialchars($_POST['publicForm']);
    ($publicForm == 'on') ? $form->publicForm = 1 : $form->publicForm = 0;
  }
  if(isset($_POST['sourcePattern'])){
    $sourcePattern = htmlspecialchars($_POST['sourcePattern']);
  }
  (isset($_POST['nameForm']) && !empty($_POST['nameForm'])) ?
  $nameForm = htmlspecialchars($_POST['nameForm'])
  : $messageError['champs_manquants'][] = 'Il manque le nom de la forme';

  (isset($_POST['author']) && !empty($_POST['author'])) ?
  $author = htmlspecialchars($_POST['author'])
  : $messageError['champs_manquants'][] = 'Il manque le nom de l\'auteur';

  (isset($_POST['patternGenerate'])) ?
  $patternGenerate = $_POST['patternGenerate']
  : $messageError['champs_manquants'][] = 'Il vous manque un pattern, le générateur est là pour ça :)';
  //Verification regex
  //Verification string
  if(isset($nameForm)){
    (preg_match($regexName,$nameForm)) ?
      $form->name = $nameForm
      : $messageError['invalidName'][] = 'nom de forme invalide';
  }
  //Verification string
  if(isset($author)){
    (preg_match($regexName,$author)) ?
      $form->author = $author
      : $messageError['invalidName'][] = 'ceci n\'est pas un nom valide';
  }
  //Verification lien
  if(isset($sourcePattern) && !empty($sourcePattern)){
    (preg_match($regexLink,$sourcePattern)) ?
    $form->sourcePattern = $sourcePattern
    : $messageError['invalidName'][] = 'ceci n\'est pas une addresse valide';
  } else {
    $form->sourcePattern = '';
  }
  if(isset($patternGenerate)){
    //Verifier format
    $preparePattern = $returnP = explode(PHP_EOL,$patternGenerate);
  }
  if (isset($_FILES['picture'])) {
    //redirection des photos téléchargées
    $repository = $_SERVER['DOCUMENT_ROOT'] . 'assets/images/forms/';
    //Verification regexImages (jpg,png,jpeg,bmp)

    //récupération de el'extension du fichier
    $nameFile = $_FILES['picture']['name'];
    //Compose le nom du chemin
    $filePicturePath = $repository . $nameFile;
    //Verifie que l'image à bien été modifiée
        if (move_uploaded_file($_FILES['picture']['tmp_name'], $filePicturePath)){
          //Hydration photo
            $form->picture = $nameFile;
            echo "DL okay";
        }
    }
    //Retour des messages d'erreurs sinon ajoute la forme
    if(isset($messageError)){
      var_dump($messageError);
    } else {
      echo 'tout est ok';

      if(isset($_POST['editForm'])){
        $form->editForm();
        //$form->editPattern($preparePattern);
      }else if($_POST['addForm']){
        $form->addForm();
        $form->addPattern($preparePattern);
      }

    }
  }
}

//Si la génération de pattern est demandé
if(isset($_POST['sentPattern'])){
  //Recuperer le pattern
  $sentpattern = $_POST['sentPattern'];
  //Le decouper en ligne dans un tableau
  $returnP = explode(PHP_EOL,$sentpattern);
  //(Formater la ligne**
  //Recompiler dans un tableau et soumettre à l'utilisateur**
  //Si l'utilisateur confirme ajouter toute les lignes au moment de l'ajout de la forme**
  //Sinon ??? proposer edition à l'utilisateur** )
}
var_dump($form->idForm);

























?>
