<?php include 'controllers/ajoutFormes.ctrl.php' ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LD - Ajouter une forme</title>
  </head>
  <body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'identificationUsers/header.php'; ?>

    <!-- Formulaire -->
    <form style="float:left" enctype='multipart/form-data' action="ajout-formes.php" method="post">
      <input name="picture" type="file" value="<?= $form->picture ?>"/><br/>
      <label for="nameForm">Nom de la forme</label>
      <input type="text" name="nameForm" value="<?= $form->name ?>"/><br/>
      <label for="sourcePattern">Source du pattern</label>
      <input type="text" name="sourcePattern" value="<?= $form->sourcePattern ?>"/><br/>
      <label for="author">Auteur du pattern</label>
      <input type="text" name="author" value="<?= $form->author ?>"><br/>
      <label for="sizeHook">Taille du crochet</label>
      <select name="sizeHook">
        <?php for($i = 1;$i <= 5;$i += 0.5 ){?>
          <option value="<?= $i ?>" <?php if($form->sizeHook == $i){echo 'selected';}?>><?= $i ?></option>
        <?php } ?>
      </select>
      <label for="sizeYarn">Taille de la laine</label>
      <select name="sizeYarn">
        <?php for($i = 1;$i <= 5;$i += 0.5 ){?>
          <option value="<?= $i ?>" <?php if($form->sizeYarn == $i){echo 'selected';}?>><?= $i ?></option>
        <?php } ?>
      </select>
      <label for="numberColors">Nombre de couleurs</label>
      <select name="numberColors">
        <?php for($i = 1;$i <= 10;$i++ ){?>
          <option value="<?= $i ?>"  <?php if($form->numberColors == $i){echo 'selected';}?>><?= $i ?></option>
        <?php } ?>
      </select><br/>
      <input type="checkbox" name="publicForm"  <?php if($form->publicForm == 1){echo 'checked';}?>/>Private<br/>

      <!-- Resultat de la génération de pattern -->
      <textarea name="patternGenerate">
      <?php if(isset($returnP)){
           foreach ($returnP as $row) {
            echo $row;
           }
         } ?>
      </textarea><br/>

      <!-- Generation de pattern -->
      <?php if(isset($_POST['idFormEdit']) || isset($_POST['editForm'])){ ?>
      <input name="idForm" value="<?= $form->idForm ?>"/>
      <input type="submit" name="editForm" value="modifier la forme"/>
      <?php }else{ ?>
        <input type="submit" name="addForm" value="ajouter à la bibliotheque"/>
      <?php } ?>
      <?php if(isset($returnP)){ ?>
        <ul>
          <?php foreach ($returnP as $row) { ?>
            <li><?= $row ?></li>
            <?php } ?>
          </ul>
          <?php } ?>
    </form>

    <!-- Générateur de pattern -->
    <form action="ajout-formes.php" method="post">
      <textarea name="sentPattern"></textarea><br/>
      <input type="submit" value="Generer">
    </form>

    <div>

    </div>
  </body>
</html>
