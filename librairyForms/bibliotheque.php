<?php include 'controllers/bibliotheque.ctrl.php' ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LD - Bibliothéque</title>
    <script src="/assets/lib/jquery/dist/jquery.min.js"></script>
    <script src="/assets/js/style.js"></script>
  <style>
  .listForms{
    display:flex;
    flex-wrap: wrap;
  }
  .zoomForms{
    position:absolute;
    top: 100px;
    left: 200px;
    width: 800px;
    background-color: white;
    min-height: 150px;
    border: 2px black solid;
  }

  </style>
</head>
  <body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'identificationUsers/header.php' ?>
    <!-- ajouter une forme -->
    <a href="ajout-formes.php"><button>Ajouter une forme</button></a>
    <a href="/projects/atelier-projets.php"><button>Creer un projet</button></a>
    <!-- liste des formes  -->
    <div class="listForms">
      <?php foreach ($formsUser as $formLibrary){ ?>
      <div style="width:200px">
        <img style="width:100%" src="/assets/images/forms/<?= $formLibrary['picture'] ?>"/>
        <!-- titre + voir le detail de la forme -->
        <h3><?= $formLibrary['name'] ?><button form="<?= $formLibrary['idForm'] ?>" class="see">voir</button></h3>
        <!-- modifier la forme -->
        <?php if($formLibrary['user_id'] == $_SESSION['idUser']){ ?>
        <form action="ajout-formes.php" method="post">
          <input type="hidden" name="idFormEdit" value="<?= $formLibrary['idForm'] ?>"/>
          <input type="submit" value="Editer"/>
        </form>
        <?php } ?>
        <!-- supprimer la forme -->
        <form action="bibliotheque.php" method="post">
          <input type="hidden" name="idFormDelete" value="<?= $formLibrary['idForm'] ?>"/>
          <input type="submit" value="Supprimer"/>
        </form>
        <!-- information de la forme -->
        <p>diff:<?= $formLibrary['difficulty'] ?> t.L:<?= $formLibrary['sizeYarn'] ?> t.C:<?= $formLibrary['sizeHook'] ?><br/>
        créer par <?= $formLibrary['author'] ?> - <a href="<?= $formLibrary['sourcePattern'] ?>">//Nom du pattern complet</a><br/>
        upload par <?php echo ($formLibrary['user_id'] == $forms->userId)? 'Vous':$formLibrary['user_id'] ?> - <?php echo ($formLibrary['publicForm'] == 0) ? 'private':'public' ?> <br/>
        </p>
      </div>
      <!-- Zomm/détail de la forme -->
      <div class="zoomForms" id="<?= $formLibrary['idForm'] ?>" style="visibility:hidden">
        <button form="<?= $formLibrary['idForm'] ?>" class="closePop">X</button>
        <?= $formLibrary['name'] ?>
        <!-- pattern -->

      </div>
      <?php } ?>
    </div>
  </body>
</html>
