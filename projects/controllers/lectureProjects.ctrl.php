<?php
session_start();
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/step.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/project.class.php';

$db = new database();
$connectDB = $db->connectDB();
$projects = new project($connectDB);
$projects->userId = $_SESSION['idUser'];
$steps = new step($connectDB);

/** Au lancement d'un projet **/

//Récupére l'id en lecture et l'attribut à l'objet
if(isset($_GET['startIdProject'])){
  $idProject = $_GET['startIdProject'];
  $_SESSION['runProject'] = $idProject;
}else{
  $idProject = $_SESSION['runProject'];
}

//Récupére les informations du projet par l'id
$projects->idProject = $idProject;
$runProject = $projects->getProjectById();

//Récupére les étapes du projet par l'id du projet
$steps->projectId = $projects->idProject;
$listSteps = $steps->getStepsByProject();

//Définit l'étape active parmis la liste d'étape du projet
foreach($listSteps as $step){
  if($step['validate'] == 0){
    //Récupére l'id de l'étape active
    $activeStep = $step['idSteps'];
    $steps->idSteps = $activeStep;
    break;
  }
}

/** Pour une étape active **/

//Récupére les détails de la forme de l'étape active via son idSteps
$runStep = $steps->getStepsById();
$steps->formId = $runStep['form_id'];
$maxSteps = sizeof($runStep) - 1;
//Récupére le pattern de l'étape du projet via l'id form commun
$runPattern = $steps->getPatternByForm();
if($runPattern){

//Définit la row Active
$activeRow = $runPattern[0]['lastRowValidate'];
}
//Récupére le nombre de row max
$nbrRows = $steps->countRowStep();

if(isset($_GET['action'])){
  if($_GET['action'] == 'done'){
    //Enregistre la row comme étant faite
    $steps->editLastCheckRow();
    header('Location:lecture-projets.php');
  }
  if($_GET['action'] == 'nextStep'){
    //Valide et enregistre l'étape comme étant faite
    $steps->editValidate();
    header('Location:lecture-projets.php');
  }
  if($_GET['action'] == 'closeProject'){
    //Ferme le projet
    $steps->cleanSteps();
    header('Location:biblio-projets.php');
  }
}

//Verifie que toute les row sont checker
//Si le lastRowValidate == la plus grande posistion

 ?>
