<?php
include $_SERVER['DOCUMENT_ROOT'].'models/step.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/project.class.php';


//Creer un projet sous l'id de l'utilisateur
$project = new project($connectDB);
$project->userId = $_SESSION['idUser'];
$stepManagement = new step($connectDB);
//si le formulaire est appelé pour l'édition
if(isset($_POST['editIdProject'])){
  $project->idProject = $_POST['editIdProject'];
  $project->hydrate();
  $stepManagement->projectId = $project->idProject;
  $listStepsId = $stepManagement->getStepsByProject();
  $steps = array();
  foreach($listStepsId as $step){
    array_push($steps,$step['form_id']);
  }
  $_SESSION['project']['steps'] = $steps;
  $_SESSION['project']['nbrColor'] = $project->nbrColor;
  $_SESSION['project']['sizeYarn'] = $project->sizeYarn;
  $_SESSION['project']['sizeHook'] = $project->sizeHook;
  $_SESSION['project']['name'] = $project->name;
  $_SESSION['project']['idProject'] = $project->idProject;

  $_SESSION['project']['mode'] = 'edit';
}

//Si le formulaire général est envoyé stocké dans l'objet['project']['sizeYarn']
if(isset($_POST['generalFormProject'])){
  $project->sizeHook = $_POST['sizeHook'];
  $project->sizeYarn = $_POST['sizeYarn'];
  $project->nbrColor = $_POST['nbrColor'];
  $_SESSION['project']['nbrColor'] = $_POST['nbrColor'];
  $_SESSION['project']['sizeYarn'] = $_POST['sizeYarn'];
  $_SESSION['project']['sizeHook'] = $_POST['sizeHook'];
}

if(isset($_SESSION['project']['steps'])){
  //Récupére le tableau projet de la session
  $steps = $_SESSION['project']['steps'];

} else {
  $steps = array();
}
if(isset($_SESSION['project']['sizeYarn'])){
  //Charge les information général du projet dans la classe
  $project->nbrColor = $_SESSION['project']['nbrColor'];
  $project->sizeYarn = $_SESSION['project']['sizeYarn'];
  $project->sizeHook = $_SESSION['project']['sizeHook'];
}
if(isset($_SESSION['project']['name'])){
  $project->name = $_SESSION['project']['name'];
  $project->idProject = $_SESSION['project']['idProject'];
}

/** Ajout de ressources vers construction **/
if(isset($_POST['addEtape'])){
  //Ajoute l'id X en tant qu"étape au tableau et enregistre dans session
  $_SESSION['project']['steps'][] = $_POST['addEtape'];
  header('Location:atelier-projets.php');
}

/** Action position étape **/
if(isset($_GET['action'])){
  //deplace un élément du tableau vers l'arriere Init
  if($_GET['action'] == 'getBack'){
      $pos = $_GET['pos'];
    if($pos != 0){
      $indexInit = $pos -1;
      $substr = array_splice($steps,$pos,1); //retire l'étape actuelle du tableau
      array_splice($steps,$indexInit, 0, $substr); //et la replace avant l'index précédent
    }
  }
  //deplace un element vers l'avant
  if($_GET['action'] == 'getNext'){
    $pos = $_GET['pos'];
    $indexInit = $pos +1;
    $substr = array_splice($steps,$pos,1); //retire l'étape actuelle du tableau
    array_splice($steps,$indexInit, 0, $substr); //et la replace avant l'index précédent
  }

  //Supprime une étape de la construction
  if($_GET['action'] == 'delete'){
    array_splice($steps,$_GET['pos'],1);
  }

  //Enregistre le tableau de la session
  $_SESSION['project']['steps'] = $steps;
}

/** Ajout à la base de donnée **/
if(isset($_POST['addProject'])){
  //ajouter projet à la base de donnée
  $project->name = $_POST['projectName'];
  $project->addProject();
  //Ajouter étapes lié au projet
  foreach($steps as $key => $step){
    $addstep = new step($connectDB);
    $addstep->position = $key;
    $addstep->formId = $step;
    $addstep->projectId = $project->idProject;
    $addstep->addStep();
    $_SESSION['project'] = array();
  }
  header('Location:biblio-projets.php');
}
var_dump($_POST);
if(isset($_POST['editProject'])){
  //Modifier le projet
  $project->name = $_POST['projectName'];
  $project->editProject();
  //Modifier les étapes
  //Ajouter étapes lié au projet
  foreach($steps as $key => $step){
    $editstep = new step($connectDB);
    $editstep->position = $key;
    $editstep->formId = $step;
    $editstep->projectId = $project->idProject;
    $maxPos = $editstep->maxPositionByProject();
    if($key > $maxPos){
      $editstep->addStep();
    }else{
      $editstep->editStepByPosition();
    }
  }
  $editstep->removeSteps();
  $_SESSION['project'] = array();
  header('Location:biblio-projets.php');

}

?>
