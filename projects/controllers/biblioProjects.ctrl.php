<?php
session_start();
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/step.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/project.class.php';


$db = new database();
$connectDB = $db->connectDB();
$projects = new project($connectDB);
$projects->userId = $_SESSION['idUser'];
$steps = new step($connectDB);
//Récupére la liste des projets public
var_dump($projects->getProjectByPublic());
//Récupére la liste des projets perso
$projectList = $projects->getProjectByUser();
//indiquer le nombre d'étape d'un projet

//Récupére une id
if(isset($_GET['idProject'])){
  $projects->idProject = $_GET['idProject'];
  //Récupére le contenu du projet
  $seeProject = $projects->getProjectById();
  //Recupére les étapes du projet
  $steps->projectId = $projects->idProject;
  $seeSteps = $steps->getStepsByProject();
    //Récupére les formes des étapes ?
}

if(isset($_GET['action']) && $_GET['action'] == 'delete'){
  //Supprimer les étapes du projets
  $steps->deleteStepsByProject();
  //Supprimer le projet
  $projects->deleteProject();
  header('Location:biblio-projets.php');
}
?>
