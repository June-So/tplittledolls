<?php include $_SERVER['DOCUMENT_ROOT'].'projects/controllers/biblioProjects.ctrl.php' ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bibliotheque Projets</title>
  </head>
  <body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'identificationUsers/header.php' ?>
    <h1>Charger un projet</h1>
    <!-- liste des projets -->
    <div id="librairyProjects">
      <?php foreach($projectList as $project){ ?>
        <?= $project['name'] ?>
        <form action="" method="get">
          <input type="hidden" name="idProject" value="<?= $project['idProject'] ?>"/>
          <button type="submit">voir</button><br/>
        </form>
      <?php } ?>
    </div>
    <!-- preview du projet selectionner -->
    <div id="previewProject">
      <!-- contenu de l'étape -->
      <?php if(isset($seeProject)){ ?>
         <div class="generalInfoProject">
           <h3><?= $seeProject['name'] ?></h3>
           <p>
             Taille crochet : <?= $seeProject['sizeHook'] ?><br/>
             Taille laine : <?= $seeProject['sizeYarn'] ?><br/>
             Nombre de couleurs : <?= $seeProject['numberColors'] ?><br/>
             Assemblé par l'Utilisateur n° <?= $seeProject['user_id'] ?><br/>
             <?php echo ($seeProject['publicProject'] == 0)? 'Private' : 'Public'; ?>
           </p>
         </div>
       <?php } ?>

      <?php if(isset($seeSteps)){
        foreach($seeSteps as $step){ ?>
          <div class="stepsProjects">
            <p>
              Forme utilisée : n°<?= $step['form_id']?>
            </p>
          </div>
          <?php } ?>
      <form action="lecture-projets.php" method="get">
        <input type="hidden" name="startIdProject" value="<?= $projects->idProject ?>" />
        <button type="submit">Commencer</button>
    </form>
      <form action="atelier-projets.php" method="post">
        <input type="hidden" name="editIdProject" value="<?= $projects->idProject ?>" />
        <button type="submit">Editer</button>
      </form>
      <a href="?idProject=<?= $projects->idProject ?>&action=delete"<button>Supprimer</button></a>
      <?php } ?>

    </div>
  </body>
</html>
