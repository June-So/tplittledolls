<?php include $_SERVER['DOCUMENT_ROOT'].'projects/controllers/lectureProjects.ctrl.php' ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LD - Run Projet</title>
  </head>
  <body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'identificationUsers/header.php'; ?>
    <!-- Informations général -->
    <div>
      <h2><?= $runProject['name'] ?></h2>
        <p>Nbre Couleurs : <?= $runProject['numberColors'] ?><br/>
        auteur : utilisateur n°<?= $runProject['user_id'] ?></p>
    </div>
    <!-- liste des étapes  -->
    <ul>
      <?php foreach ($listSteps as $stepResume) { ?>
        <li>Forme n°<?= $stepResume['form_id'] ?> - checked : <?php echo ($stepResume['validate'] == 0)?'non':'oui'?></li>
      <?php }?>
    </ul>
    <!-- forme/étape en cours -->
    <div id="runStep">
      Finis ? <?= $runStep['validate'] ?><br/>
      Derniere row validé <?= $runStep['lastRowValidate'] ?><br/>
      TC : <?= $runStep['sizeHook']?> - TL : <?= $runStep['sizeYarn'] ?><br/>
      Nombre de couleurs : <?= $runStep['numberColors']?><br/>
      <h3><?= $runStep['name'] ?></h3>
      Auteur : <?= $runStep['author']?><br/>
      source : <a href="<?= $runStep['sourcePattern'] ?>">Aller voir</a>
    </div>
      <!-- pattern -->
    <div id="runPattern">
      <ul>
        <?php foreach ($runPattern as $runRow) { ?>
          <li>
            <?php if($activeRow > $runRow['rowPosition']){
              echo 'fait!';
            }elseif($activeRow == $runRow['rowPosition']){
              echo'ACTIVE';
            }else{
              echo 'en attente';
            } ?>
            <?= $runRow['rowPosition'] ?> - <?= $runRow['rowtext'] ?> - Color N°<?= $runRow['colorRank'] ?>
          </li>
        <?php }?>
      </ul>
      <?php if($runStep['position'] == $maxSteps){ ?>
        <a href="?action=closeProject"><button>Finir Projet</button></a>
      <?php } else {
        if($nbrRows+1 == $activeRow){ ?>
          <a href="?action=nextStep"><button>next</button></a>
          <?php }else{ ?>
            <a href="?action=done"><button>done</button></a>
            <?php } ?>
          <?php }?>

    </div>
  </body>
</html>
