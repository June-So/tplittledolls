
<?php
include $_SERVER['DOCUMENT_ROOT'].'librairyForms/controllers/bibliotheque.ctrl.php' ?>
<?php include $_SERVER['DOCUMENT_ROOT'].'projects/controllers/atelierProjets.ctrl.php' ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>LD - Atelier Projets</title>
    <script src="/assets/lib/jquery/dist/jquery.min.js"></script>
    <script src="/assets/js/style.js"></script>
  </head>
  <style>
  #atelier-projets{
    display:flex;
    flex-wrap: wrap;
  }
  #ressources{
    width: 280px;
    border: 2px solid black;
  }
  .listForms{
    display:flex;
    flex-wrap: wrap;
    height: 96vh;
    overflow: scroll;
  }
  .formUnite{
    width: 80px;
  }
  .zoomForms{
    position:absolute;
    top: 100px;
    left: 200px;
    width: 800px;
    background-color: white;
    min-height: 150px;
    border: 2px black solid;
  }

  #building{
    border: 2px solid black;
    width: 700px;
  }

  #formSteps{
    display:flex;
    flex-wrap: wrap;
  }

  #AdvencedEditing{
    width: 280px;
    border: 2px solid black;

  }
  </style>
  <body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'identificationUsers/header.php' ?>

    <div id="atelier-projets">
    <!-- Module de ressources -->
    <div id="ressources">
      <div class="listForms">
        <?php foreach ($formsUser as $formLibrary){ ?>
        <div class="formUnite">
          <img style="width:100%" src="/assets/images/forms/<?= $formLibrary['picture'] ?>"/>
          <!-- titre + voir le detail de la forme -->
          <h3><?= $formLibrary['name'] ?><button form="<?= $formLibrary['idForm'] ?>" class="see">voir</button></h3>
          <!-- utiliser la forme -->
          <form action="" method="post">
            <input name="addEtape" value="<?= $formLibrary['idForm'] ?>"/>
            <button type="submit">utilisé</button>
          </form>
          <!-- information de la forme -->
          <p>diff:<?= $formLibrary['difficulty'] ?> t.L:<?= $formLibrary['sizeYarn'] ?> t.C:<?= $formLibrary['sizeHook'] ?><br/>
          </p>
        </div>
        <!-- Zomm/détail de la forme -->
        <div class="zoomForms" id="<?= $formLibrary['idForm'] ?>" style="visibility:hidden">
          <button form="<?= $formLibrary['idForm'] ?>" class="closePop">X</button>
          <?= $formLibrary['name'] ?>
          <!-- pattern -->
        </div>
        <?php } ?>
      </div>
    </div>

    <!-- Partie de construction -->
    <div id="building">

      <!-- Formulaire général -->
      <form action="" method="post">
        <label for="sizeHook">Taille crochet</label>
        <select name="sizeHook">
          <option value="" <?php echo (isset($_POST['sizeHook']) && $_POST['sizeHook'] == "")? 'selected' : '' ?>>--</option>
          <option <?php echo (isset($_POST['sizeHook']) && $_POST['sizeHook'] == 1)? 'selected' : '' ?>>1</option>
          <option <?php echo (isset($_POST['sizeHook']) && $_POST['sizeHook'] == 2)? 'selected' : '' ?>>2</option>
        </select>
        <label for="sizeYarn">Taille Laine</label>
        <select name="sizeYarn">
          <option value="" <?php echo (isset($_POST['sizeYarn']) && $_POST['sizeYarn'] == "")? 'selected' : '' ?>>--</option>
          <option <?php echo (isset($_POST['sizeYarn']) && $_POST['sizeYarn'] == 2)? 'selected' : '' ?>>2</option>
          <option <?php echo (isset($_POST['sizeYarn']) && $_POST['sizeYarn'] == 3)? 'selected' : '' ?>>3</option>
        </select>
        <label for="nbrColor">Nombre Couleurs</label>
        <select name="nbrColor">
          <option <?php echo (isset($_POST['nbrColor']) && $_POST['nbrColor'] == 1)? 'selected' : '' ?>>1</option>
          <option <?php echo (isset($_POST['nbrColor']) && $_POST['nbrColor'] == 2)? 'selected' : '' ?>>2</option>
          <option <?php echo (isset($_POST['nbrColor']) && $_POST['nbrColor'] == 3)? 'selected' : '' ?>>3</option>
        </select><br/>
        <p>Couleur Principale</p>
        <?php if(isset($_POST['nbrColor']) && $_POST['nbrColor'] >= 2){ ?>
          <p>Couleur Secondaire</p>
        <?php } ?>
        <?php if(isset($_POST['nbrColor']) && $_POST['nbrColor'] >= 3){ ?>
          <p>Troisieme couleur</p>
        <?php } ?>
        <input name="general" type="checkbox"><label for="general">Appliquer les changement au étapes déjà personalisé</label><br/>
        <input type="submit" name="generalFormProject" value="appliquer"/><br/>
      </form>

      <!-- Affichage/Formulaire étape -->
      <div id="formSteps">
      <?php foreach($steps as $key => $step){ ?>
      <div>
        <img href=""/>
        <h3> Id : <?= $step ?></h3>
        <p>TC: <?= $project->sizeHook ?> - TL: <?= $project->sizeYarn ?><br/>
        modifier C <?php for($i = 1; $i <= $project->nbrColor; $i++ ){ ?> []<?php } ?></p>
        <a href="?pos=<?=$key?>&action=getBack"><button> <= </button></a>
        <a href="?pos=<?=$key?>&action=delete"><button> X </button></a>
        <a href="?pos=<?=$key?>&action=getNext"><button> => </button></a>

      </div>
      <?php } ?>
    </div>
    <?php if($_SESSION['project']['mode'] == 'edit'){ ?>
      <form action="" method="post">
        <label>Renommez votre projet</label>
        <input type="texte" value="<?= $project->name ?>" name="projectName" />
        <input type="submit" name="editProject" value="Modifier projet">
      </form>
    <?php } else { ?>
    <form action="" method="post">
      <label>Donnez un nom à votre projet</label>
      <input type="texte" name="projectName" />
      <input type="submit" name="addProject" value="Creer projet">
    </form>
  <?php } ?>
  </div>

    <!-- Partie d'édition avancé -->
    <div id="AdvencedEditing">
    </div>
  </div>
  </body>
</html>
