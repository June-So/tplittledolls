<?php
//(la vérification des champs doit se faire pendant la saisie)
//Quand le formulaire est soumis et conforme
//enregistre l'utilisateur dans la base user
$database = new database();
$connectDB = $database->connectDB();
$user = new user($connectDB);

$regexPseudo = '/^[A-Za-z0-9-_]+$/';
$regexEmail = '/^[a-zA-Z]([\.\-_]?[a-zA-Z]+)*[a-zA-Z][@]([a-zA-Z]+[\.]){1,2}[a-z]+/';

if(count($_POST) > 0){
  if(isset($_POST['login'])){
    $loginForm = htmlspecialchars($_POST['login']);
  }
  if(isset($_POST['email'])){
    $emailForm = htmlspecialchars($_POST['email']);
  }
  if(isset($_POST['password']) && !empty($_POST['password'])){
    $passwordForm = sha1($_POST['password']);
  }
  if(isset($_POST['verifPassword'])){
    $verifPassword = sha1($_POST['verifPassword']);
  }
}

$messageError = ['ok'];

if(count($_POST) > 0){
  // Vérification de l'existence remplit du login, sinon stock une erreur
  if(isset($loginForm) && !empty($loginForm)){
    //Enreigstre le pseudo si il est vaide sinon stock une erreur
    (preg_match($regexPseudo,$loginForm)) ? $user->pseudo = $loginForm
                                          :$messageError[] = 'Pseudo invalide';
    //Vérifie la disponibilité du pseudo;
    ($user->availablePseudo() == false) ? $user->pseudo = $loginForm
                                        : $messageError[0] = 'Ce pseudo n\'est pas disponible';
  } else {
    $messageError[0] = 'Des champs sont inexistants';
}

  // Vérification de l'existence remplit de l'email, sinon stock une erreur
  if(isset($emailForm) && !empty($emailForm)){
    //Enregistre l'email si il est valide sinon stock une erreur
    (preg_match($regexEmail,$emailForm)) ? $user->email = $emailForm
                                        : $messageError[] = 'Email invalide';
    //Vérifie la disponibilité d'un email;
    ($user->availableEmail() == false) ? $user->email = $emailForm
                                        : $messageError[] = 'Cette addresse est déjà utilisé';
  } else{
    $messageError[0] = 'Des champs sont inexistants';
  }

    // Vérification de l'exsitence remplit du password et de sa verification sinon stock une erreur
  (isset($verifPassword) && !empty($verifPassword) && isset($passwordForm) && !empty($passwordForm)) ?
    ($verifPassword == $passwordForm) ? $user->password = $verifPassword
                                      : $messageError[] = 'Vos mots de passe sont différents'
  : $messageError[0] = 'Des champs sont inexistants';


//Si aucune erreur dans le formulaire, ajouter à la base de donnée
if($messageError == ['ok']){
$user->addUser();
echo 'Inscription réussie, vous pouvez dès à present vous connecter <a href="index.php">en revant ici</a>';
} else {
  var_dump($messageError);
}

}
 ?>
