<?php
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/user.class.php';


  $database = new database();
  $connectDB = $database->connectDB();
  $user = new user($connectDB);

  $regexPseudo = '/^[A-Za-z0-9-_]+$/';
  //Hydration post -> attribut
  if(count($_POST) > 0){
  if(isset($_POST['login'])){
    $login = htmlspecialchars($_POST['login']);
    $user->pseudo = $login;
  }
  if(isset($_POST['password'])){
    $password = sha1($_POST['password']);
    $user->password = $password;
  }

  //Verification existence pseudo+mdp
  $id = $user->getIdByPasswordAndPseudo();
  //Si le combo correspond alors enregistré l'id dans la session et redirige vers l'acceuil
  if($id){
    $messageError[] = 'Vous êtes connecté !';
    $_SESSION['idUser'] = $id['idUser'];
    $_SESSION['pseudo'] = $user->pseudo;
    header('Location:home.php');
  }else{
  //Sinon stock une erreur
    $messageError[] = 'identifiant ou mot de passe invalide';
  }
  var_dump($messageError);
}

//Deconnexion
if(isset($_GET['deconnexion'])){
  $_SESSION = array();
  session_destroy();
}

 ?>
