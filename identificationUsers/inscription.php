<?php
include $_SERVER['DOCUMENT_ROOT'].'models/database.class.php';
include $_SERVER['DOCUMENT_ROOT'].'models/user.class.php';
include 'controllers/inscription.ctrl.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Little Dolls</title>
  </head>
  <body>
    <!-- header -->
    <!-- Formulaire d'inscription -->
    <form action="inscription.php" method="post">
      <label for="login">Choisissez un pseudo</label>
      <input type="texte" name="login"/><br/>
      <label for="email">Entrez votre adresse mail</label>
      <input type="texte" name="email"/><br/>
      <label for="password">Choisissez un mot de passe</label>
      <input type="password" name="password"/><br/>
      <label for="verifPassword">Confirmer votre mot de passe</label>
      <input type="password" name="verifPassword"/><br/>
      <input type="submit" value="S'inscrire"/>
    </form>
    <p><a href="index.php">Retourner à l'index</a></p>
  </body>
</html>
