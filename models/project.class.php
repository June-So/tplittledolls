<?php
  class project{
    public $difficulty;
    public $idProject;
    public $name;
    public $nbrColor = 1;
    public $sizeHook;
    public $sizeYarn;
    public $userId;
    private $db;

    public function __construct($db){
      $this->db = $db;
    }

    public function addProject(){
      $req = $this->db->prepare('INSERT INTO `LD_projects`(`name`,`sizeYarn`,`sizeHook`,`numberColors`,`user_id`) VALUES (:name,:sizeYarn,:sizeHook,:numberColors,:user_id)');
      $req->bindValue('name',$this->name,PDO::PARAM_STR);
      $req->bindValue('sizeYarn',$this->sizeYarn,PDO::PARAM_INT);
      $req->bindValue('sizeHook',$this->sizeHook,PDO::PARAM_INT);
      $req->bindValue('numberColors',$this->nbrColor,PDO::PARAM_INT);
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->execute();
      $this->idProject = $this->db->lastInsertId();
    }

    public function getProjectByPublic(){
      $req = $this->db->prepare('SELECT * FROM `LD_projects` WHERE publicProject = 1 AND user_id <> :user_id');
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->execute();
      return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProjectByUser(){
      $req = $this->db->prepare('SELECT * FROM `LD_projects` WHERE user_id = :user_id');
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->execute();
      return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProjectById(){
      $req = $this->db->prepare('SELECT * FROM `LD_projects` WHERE idProject = :idProject AND (user_id = :user_id OR publicProject = 1)');
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->bindValue('idProject',$this->idProject,PDO::PARAM_INT);
      $req->execute();
      return $req->fetch(PDO::FETCH_ASSOC);
    }

    public function editProject(){
      $req = $this->db->prepare('UPDATE `LD_projects` SET name = :name,sizeYarn = :sizeYarn, sizeHook = :sizeHook,numberColors = :numberColors WHERE idProject = :idProject');
      $req->bindValue('name',$this->name,PDO::PARAM_STR);
      $req->bindValue('sizeYarn',$this->sizeYarn,PDO::PARAM_INT);
      $req->bindValue('sizeHook',$this->sizeHook,PDO::PARAM_INT);
      $req->bindValue('numberColors',$this->nbrColor,PDO::PARAM_INT);
      $req->bindValue('idProject',$this->idProject,PDO::PARAM_INT);
      return $req->execute();
    }
    public function deleteProject(){
      $req = $this->db->prepare('DELETE FROM `LD_projects` WHERE idProject = :idProject');
      $req->bindValue('idProject',$this->idProject,PDO::PARAM_INT);
      return $req->execute();
    }

    public function hydrate(){
      $req = $this->db->prepare('SELECT * FROM `LD_projects` WHERE idProject = :idProject AND (user_id = :user_id OR publicProject = 1)');
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->bindValue('idProject',$this->idProject,PDO::PARAM_INT);
      $req->execute();
      $project = $req->fetch(PDO::FETCH_ASSOC);

      $this->name = $project['name'];
      $this->nbrColor = $project['numberColors'];
      $this->sizeHook = $project['sizeHook'];
      $this->sizeYarn = $project['sizeYarn'];
      $this->userId = $project['user_id'];
    }
  }
?>
