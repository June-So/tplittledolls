<?php
class step{
  //Nécessaire !!
  public $idSteps;
  public $projectId;
  public $formId;
  public $position;
  //a ajouter dans la bdd si nécessaire
  public $sizeYarn;
  public $sizeHook;
  public $nbrColors;
  //Nécessaire pour la lecture
  public $validate;
  public $lasRowValidate;
  private $db;

  public function __construct($db){
    $this->db = $db;
  }

  public function addStep(){
    $req = $this->db->prepare('INSERT INTO `LD_steps`(`project_id`,`form_id`,`position`) VALUES(:project_id,:form_id,:position)');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
    $req->bindValue('position',$this->position,PDO::PARAM_INT);
    return $req->execute();
  }

/**
 *utilise form_id, idSteps, validate
 */
  public function getStepsByProject(){
    $req = $this->db->prepare('SELECT * FROM `LD_steps` WHERE project_id = :project_id');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getStepsById(){
    $req = $this->db->prepare('SELECT * FROM `LD_steps` INNER JOIN `LD_forms` ON `LD_steps`.`form_id` = `LD_forms`.`idForm` WHERE `idSteps` = :idSteps');
    $req->bindValue('idSteps',$this->idSteps,PDO::PARAM_INT);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  public function deleteStepsByProject(){
    $req = $this->db->prepare('DELETE FROM `LD_steps` WHERE project_id = :project_id');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->execute();
  }

  public function getPatternByForm(){
    $req = $this->db->prepare('SELECT `lastRowValidate`,`rowPosition`,`rowtext`,`colorRank` FROM `LD_steps` INNER JOIN `LD_patterns` ON `LD_patterns`.`form_id` = `LD_steps`.`form_id` WHERE idSteps = :idSteps ORDER BY rowPosition');
    $req->bindValue('idSteps',$this->idSteps,PDO::PARAM_INT);
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function editLastCheckRow(){
    $req = $this->db->prepare('UPDATE `LD_steps` SET `lastRowValidate` = `lastRowValidate`+ 1 WHERE `idSteps` = :idSteps');
    $req->bindValue('idSteps',$this->idSteps,PDO::PARAM_INT);
    return $req->execute();
  }

  public function editValidate(){
    $req = $this->db->prepare('UPDATE `LD_steps` SET `validate` = 1 WHERE `idSteps` = :idSteps');
    $req->bindValue('idSteps',$this->idSteps,PDO::PARAM_INT);
    return $req->execute();
  }

  public function editStepByPosition(){
    $req = $this->db->prepare('UPDATE `LD_steps` SET `form_id`=:form_id WHERE `project_id` = :project_id AND `position`=:position');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
    $req->bindValue('position',$this->position,PDO::PARAM_INT);
    return $req->execute();
  }

  public function removeSteps(){
    $req = $this->db->prepare('DELETE FROM `LD_steps` WHERE `project_id` = :project_id AND `position` > :position');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->bindValue('position',$this->position,PDO::PARAM_INT);
    return $req->execute();
  }

  public function cleanSteps(){
    $req = $this->db->prepare('UPDATE `LD_steps` SET `validate` = 0, `lastRowValidate` = 0 WHERE `project_id` = :project_id' );
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    return $req->execute();
  }

  public function maxPositionByProject(){
    $req = $this->db->prepare('SELECT SUM(position) FROM `LD_steps` WHERE `project_id` = :project_id');
    $req->bindValue('project_id',$this->projectId,PDO::PARAM_INT);
    $req->execute();
    $maxPos = $req->fetch(PDO::FETCH_ASSOC);
    return $maxPos['SUM(position)'];
  }
  /** Methode pattern**/
  public function countRowStep(){
    $req = $this->db->prepare('SELECT COUNT(`rowPosition`) FROM `LD_patterns` WHERE `form_id` = :form_id');
    $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
    $req->execute();
    $count = $req->fetch(PDO::FETCH_ASSOC);
    return $count['COUNT(`rowPosition`)'];
  }
}
?>
