<?php
/**
 * @method addUser() ajoute un utilisateur à la base de donnée
 * @method availablePseudo() Vérifie la disponibilité d'un pseudo
 * @method availableEmail() Vérifie la disponibilité d'un email
 */
class user{
  private $id;
  public $pseudo;
  public $password;
  public $email;
  private $db;
  //Date inscription
  //Derniere connexion

/**
 *@param objet pdo, objet de connexion à la base de donnée
 */
  public function __construct($sb){
    $this->db = $sb;
  }

  /**
   * Ajoute un utilisateur à la base de donnée
   * @return bool succes de l'insertion de la requête
   */
  public function addUser(){
    $req = $this->db->prepare('INSERT INTO `LD_users`(`pseudo`,`password`,`dateInscription`,`email`) VALUES (:pseudo,:password,NOW(),:email)');
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    $req->bindValue('password',$this->password,PDO::PARAM_STR);
    $req->bindValue('email',$this->email,PDO::PARAM_STR);
    return $req->execute();
  }

  /**
   * Vérifie la disponibilité d'un pseudo
   * @return str un pseudo ou false si libre
   */
  public function availablePseudo(){
    $req = $this->db->prepare('SELECT `pseudo` FROM `LD_users` WHERE pseudo = :pseudo');
    $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  /**
   * Vérifie la disponibilité d'un email
   * @return str un email ou false si l'email est libre
   */
  public function availableEmail(){
    $req = $this->db->prepare('SELECT `email` FROM `LD_users` WHERE email = :email');
    $req->bindValue('email',$this->email,PDO::PARAM_STR);
    $req->execute();
    return $req->fetch(PDO::FETCH_ASSOC);
  }

  /**
   * Vérifie si la combinaison pseudo + mdp existe
   */
   public function getIdByPasswordAndPseudo(){
     $req = $this->db->prepare('SELECT `idUser` FROM `LD_users` WHERE pseudo = :pseudo AND password = :password');
     $req->bindValue('pseudo',$this->pseudo,PDO::PARAM_STR);
     $req->bindValue('password',$this->password,PDO::PARAM_STR);
     $req->execute();
     return $req->fetch(PDO::FETCH_ASSOC);
   }
}
?>
