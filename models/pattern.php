<?php
class pattern{
  private $db;
  public $formId;
  public function __construct($db){
    $this->db = $db;
  }
  /**
   * Controller Ajout-forme
   * Ajout un pattern à la base de donnée lié avec l'id de la forme actuelle
   * @param array Rows du pattern séparées
   * @return bool succés de toutes les lignes ajoutées
   */
  public function addPattern($arrayPattern){
    $rowPos = 1;
    foreach ($arrayPattern as $row) {
      $req = $this->db->prepare('INSERT INTO `LD_patterns`(`rowPosition`,`rowtext`,`form_id`,`colorRank`) VALUES (:rowPosition,:rowtext,:form_id,:colorRank)');
      $req->bindValue('rowPosition',$rowPos++,PDO::PARAM_INT);
      $req->bindValue('rowtext',$row,PDO::PARAM_STR);
      $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
      $req->bindValue('colorRank',1,PDO::PARAM_INT);
      $succed = $req->execute();
      if($succed == false){
        $error = true;
      }
    }
    (isset($error)) ? $succeds = false : $succeds = true;
    return $succeds;
  }

  /**
   *edit tout les lignes des patterns depuis un tableau
   * @param array $arrayPattern, liste des Rows
   * @return succes général de la modification des rows ds la bdd
   */
  public function editPattern($arrayPattern){
    $rowPos = 1;
    foreach ($arrayPattern as $row) {
      $req = $this->db->prepare('UPDATE `LD_patterns` SET `rowPosition`=:rowPosition,`rowtext`=:rowtext,`colorRank`=:colorRank WHERE form_id = :form_id');
      $req->bindValue('rowPosition',$rowPos++,PDO::PARAM_INT);
      $req->bindValue('rowtext',$row,PDO::PARAM_STR);
      $req->bindValue('colorRank',1,PDO::PARAM_INT);
      $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
      $succed = $req->execute();
      if($succed == false){
        $error = true;
      }
    }
    (isset($error)) ? $succeds = false : $succeds = true;
    return $succeds;
  }

  public function deletePattern(){
    $req = $this->db->prepare('DELETE FROM `LD_patterns` WHERE `form_id` = :form_id');
    $req->bindValue('form_id',$this->formId,PDO::PARAM_INT);
    return $req->execute();
  }
}
 ?>
