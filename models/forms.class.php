<?php
/**
 * @method getFormByUser() récupére la liste des formes d'un utilisateur
 * @method getFormForUser() récupére tout les formes que l'utilisateur peut voir
 * @method editForm() édit la forme dans la bdd
 * @method loadFormById() Import la forme appartenant à son utlisateur partir de son id et hydrate l'objet
 * @method addForm() Ajoute la forme à la bdd
 * @method deleteForm() Supprime une forme
 */
class form{
  public $idForm;
  public $name;
  private $difficulty;
  public $sourcePattern;
  public $author;
  public $picture;
  public $sizeYarn;
  public $sizeHook;
  public $numberColors;
  public $userId;
  public $publicForm = 0;
  private $db;

  public function __construct($db,int $userId = null){
    $this->db = $db;
    if($userId != null){
      $this->userId = $userId;
    }
  }

/**
 * récupére la liste des formes d'un utilisateur
 * @return array liste des formes d'un utlisateur
 */
  public function getFormByUser(){
    $req = $this->db->prepare('SELECT * FROM `LD_forms` WHERE user_id = :user_id');
    $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
    $req->execute();
    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * récupére la liste des formes d'un utilisateur + formes commune
   * @return array liste des formes d'un utlisateur
   */
    public function getFormForUser(){
      $req = $this->db->prepare('SELECT * FROM `LD_forms` WHERE user_id = :user_id OR publicForm = 1');
      $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
      $req->execute();
      return $req->fetchAll(PDO::FETCH_ASSOC);
    }


  /**
   * modifie une forme
   * @return le succed de la modification d'une forme
   */
  public function editForm(){
    $req = $this->db->prepare('UPDATE `LD_forms` SET `name` = :name,`sourcePattern`= :sourcePattern,`author`=:author,`sizeYarn`=:sizeYarn,`sizeHook`=:sizeHook,`picture`=:picture,`numberColors`=:numberColors,`publicForm`=:publicForm WHERE idForm = :idForm');
    $req->bindValue('name',$this->name,PDO::PARAM_STR);
    $req->bindValue('picture',$this->picture,PDO::PARAM_STR);
    $req->bindValue('sourcePattern',$this->sourcePattern,PDO::PARAM_STR);
    $req->bindValue('author',$this->author,PDO::PARAM_STR);
    $req->bindValue('sizeYarn',$this->sizeYarn,PDO::PARAM_INT);
    $req->bindValue('sizeHook',$this->sizeHook,PDO::PARAM_INT);
    $req->bindValue('numberColors',$this->numberColors,PDO::PARAM_INT);
    $req->bindValue('publicForm',$this->publicForm,PDO::PARAM_STR);
    $req->bindValue('idForm',$this->idForm,PDO::PARAM_INT);
    return $req->execute();
  }

  /**
   * Charge une forme à partir de son id et hydrate l'objet, il faut qu'elle appartienne à l'utilisateur
   * @return les champs d'une forme
   */
  public function loadFormById(){
    $req = $this->db->prepare('SELECT * FROM `LD_forms` WHERE idForm = :idForm AND user_id = :user_id');
    $req->bindValue('idForm',$this->idForm,PDO::PARAM_INT);
    $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
    $req->execute();
    $form = $req->fetch(PDO::FETCH_ASSOC);

    $this->name = $form['name'];
    $this->sourcePattern = $form['sourcePattern'];
    $this->author = $form['author'];
    $this->picture = $form['picture'];
    $this->sizeYarn = $form['sizeYarn'];
    $this->sizeHook = $form['sizeHook'];
    $this->numberColors = $form['numberColors'];
    $this->publicForm = $form['publicForm'];

    return $form;
  }

  /**
   * Ajoute une forme à la base de donnée et récupére l'id de cette forme
   * @return bool succed de l'ajout;
   */
  public function addForm(){
    $req = $this->db->prepare('INSERT INTO `LD_forms` (`name`,`sourcePattern`,`author`,`sizeYarn`,`sizeHook`,`picture`,`numberColors`,`user_id`,`publicForm`) VALUES (:name,:sourcePattern,:author,:sizeYarn,:sizeHook,:picture,:numberColors,:user_id,:publicForm)');
    $req->bindValue('name',$this->name,PDO::PARAM_STR);
    $req->bindValue('picture',$this->picture,PDO::PARAM_STR);
    $req->bindValue('sourcePattern',$this->sourcePattern,PDO::PARAM_STR);
    $req->bindValue('author',$this->author,PDO::PARAM_STR);
    $req->bindValue('sizeYarn',$this->sizeYarn,PDO::PARAM_INT);
    $req->bindValue('sizeHook',$this->sizeHook,PDO::PARAM_INT);
    $req->bindValue('numberColors',$this->numberColors,PDO::PARAM_INT);
    $req->bindValue('user_id',$this->userId,PDO::PARAM_INT);
    $req->bindValue('publicForm',$this->publicForm,PDO::PARAM_STR);
    $succed = $req->execute();
    if($succed){
      $this->idForm = $this->db->lastInsertId();
    }
    return $succed;
  }

  /**
   * Supprime une forme
   */
   public function deleteForm(){
     $req = $this->db->prepare('DELETE FROM `LD_forms` WHERE idForm = :idForm');
     $req->bindValue('idForm',$this->idForm,PDO::PARAM_INT);
     $req->execute();
   }


/** A NETTOYER -> METHODE CLASSE PATTERN **/
  /**
   * Controller Ajout-forme
   * Ajout un pattern à la base de donnée lié avec l'id de la forme actuelle
   * @param array Rows du pattern séparées
   * @return bool succés de toutes les lignes ajoutées
   */
  public function addPattern($arrayPattern){
    $rowPos = 1;
    foreach ($arrayPattern as $row) {
      $req = $this->db->prepare('INSERT INTO `LD_patterns`(`rowPosition`,`rowtext`,`form_id`,`colorRank`) VALUES (:rowPosition,:rowtext,:form_id,:colorRank)');
      $req->bindValue('rowPosition',$rowPos++,PDO::PARAM_INT);
      $req->bindValue('rowtext',$row,PDO::PARAM_STR);
      $req->bindValue('form_id',$this->idForm,PDO::PARAM_INT);
      $req->bindValue('colorRank',1,PDO::PARAM_INT);
      $succed = $req->execute();
      if($succed == false){
        $error = true;
      }
    }
    (isset($error)) ? $succeds = false : $succeds = true;
    return $succeds;
  }

  /**
   *edit tout les lignes des patterns depuis un tableau
   * @param array $arrayPattern, liste des Rows
   * @return succes général de la modification des rows ds la bdd
   */
  public function editPattern($arrayPattern){
    $rowPos = 1;
    foreach ($arrayPattern as $row) {
      $req = $this->db->prepare('UPDATE `LD_patterns` SET `rowPosition`=:rowPosition,`rowtext`=:rowtext,`colorRank`=:colorRank WHERE form_id = :form_id');
      $req->bindValue('rowPosition',$rowPos++,PDO::PARAM_INT);
      $req->bindValue('rowtext',$row,PDO::PARAM_STR);
      $req->bindValue('colorRank',1,PDO::PARAM_INT);
      $req->bindValue('form_id',$this->idForm,PDO::PARAM_INT);
      $succed = $req->execute();
      if($succed == false){
        $error = true;
      }
    }
    (isset($error)) ? $succeds = false : $succeds = true;
    return $succeds;
  }
}
?>
