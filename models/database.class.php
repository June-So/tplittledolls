<?php
class database{

  private $host = 'localhost';
  private $username = 'dollys';
  private $password = 'kupo';
  private $database = 'littledolls';
  private $db;


  /**
    *@param paramétre de connexion, null par défaut
    */
  public function __construct($host = null, $username = null, $password = null, $database = null){
    if($host != null){
      $this->host = $host;
      $this->username = $username;
      $this->password = $password;
      $this->database = $database;
    }
    //connexion à la base de donnée
    $this->db = $this->connectDb();
  }

  /**
   *connexion à la base de donnée
   *@return objet pdo
   */
  public function connectDB(){
    try{
    return new PDO('mysql:host='.$this->host.';dbname='.$this->database.';charset=utf8',$this->username,$this->password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }catch(Exception $e){
      die('Erreur de connexion à la base'. $e->getMessage());
    }
  }

}
?>
